<?php

class NewsPage extends Page
{

    private static $db = array(
        "Date" => "SS_Datetime"
    );
    private static $allowed_children = "none";
    private static $default_parent = "NewsArchive";
    private static $can_be_root = false;
    private static $show_in_sitetree = false;

    private static $defaults = array(
        'ShowInMenus' => false
    );

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
        $datefield = new DatetimeField(
            "Date",
            "Julkaisupäivä (listaukset perustuvat tähän, tämä EI rajoita uutisen näkyvyyttä)"
        );
        $datefield->getDateField()->setConfig("showcalendar", true);
        $fields->addFieldToTab("Root.Main", $datefield, "Content");

        return $fields;
    }

    public function getCMSValidator()
    {
        return new RequiredFields("Date");
    }

    protected function onBeforeWrite()
    {
        parent::onBeforeWrite();
        if ($this->Embargo) {
            $this->Date = $this->Embargo;
        }
    }
}

class NewsPage_Controller extends Page_Controller
{
}
