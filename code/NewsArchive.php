<?php

/**
 * Class NewsArchive
 *
 * @property bool ShowContent
 */
class NewsArchive extends Page
{
    private static $db = array(
        "ShowContent" => "Boolean"
    );

    private static $defaults = array(
        "ShowContent" => 1
    );

    private static $allowed_children = array("NewsPage");

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        /** @var CheckboxField $checkbox */
        $fields->addFieldToTab(
            "Root.Main",
            CheckboxField::create(
                "ShowContent",
                _t("NewsArchive.ShowContent", "Show content")
            ),
            "Content"
        );

        return $fields;
    }

    public function stageChildren($showAll = false)
    {
        $staged = NewsPage::get()
            ->filter('ParentID', (int)$this->owner->ID)
            ->sort("Date", "DESC");
        if (!$showAll && $this->owner->db('ShowInMenus')) {
            $staged = $staged->filter('ShowInMenus', 1);
        }
        $this->owner->extend("augmentStageChildren", $staged, $showAll);

        return $staged->exclude("ClassName", ["NewsPage" => "NewsPage"]);
    }

    public function getLumberjackTitle()
    {
        return _t('NewsArchive.NewsItems', 'Uutiset');
    }
}

class NewsArchive_Controller extends Page_Controller
{

    private static $allowed_actions = array(
        'rss',
        'archiveByYear',
        'YearForm'
    );
    private static $url_handlers = array(
        'year/$Year' => 'index'
    );

    public function rss()
    {
        $news = NewsPage::get()
            ->sort("Date", "DESC")
            ->limit(20);
        $updated = $news->max("Date");
        $feed = new RSSFeed(
            $entries = $news,
            $link = $this->Link("rss"),
            $title = $this->Title,
            $description = null,
            $titleField = "Title",
            $descriptionField = null,
            $authorField = null,
            $lastModified = $updated
        );
        return $feed->outputToBrowser();
    }

    public function getAllYears()
    {
        $q = NewsPage::get()->dataQuery()->query();
        $idField = $q->expressionForField('ID');
        $localeField = $q->expressionForField('Locale');
        $classnameField = $q->expressionForField('ClassName');
        $q->setSelect(array());
        $q->selectField('YEAR("Date")', 'Year');
        $q->selectField($idField);
        $q->selectField($localeField);
        $q->selectField($classnameField);
        $q->setGroupBy('"Year"');
        $q->setOrderBy('"Year" DESC');

        $years = new ArrayList();
        foreach ($q->execute() as $record) {
            $year = $record['Year'];
            $years->push(
                array(
                    'Year' => $year
                )
            );
        }
        $current_year = date('Y');
        if (isset($years[0]) && $years[0]['Year'] != $current_year) {
            $years->unshift(
                array(
                    'Year' => $current_year
                )
            );
        }
        return $years;
    }

    public function YearForm()
    {
        $years = $this->getAllYears();
        $fields = new FieldList();

        $field = new DropdownField(
            'Year',
            _t('NewsArchive.Year', 'Year'),
            $years->map('Year', 'Year'),
            (int)$this->request->param('Year')
        );
        $fields->push($field);

        $actions = new FieldList(
            $action = FormAction::create('YearFilter')
        );
        $action->setTitle(_t('NewsArchive.Filter', 'Filter'));

        $form = new Form($this, 'YearForm', $fields, $actions);
        $form->disableSecurityToken();
        return $form;
    }

    public function YearFilter($data, Form $form)
    {
        $year = (int)$data['Year'];
        $url = $this->Link('year');
        $url = $this->join_links($url, $year);
        $this->redirect($url);
    }

    public function AllChildren()
    {
        $year = (int)$this->request->param('Year');
        if ($year < 2000) {
            $year = date('Y');
        }

        return $this->dataRecord->AllChildren()->filter(
            'Date:StartsWith',
            $year
        );
    }
}
