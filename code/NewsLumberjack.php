<?php
/**
 * Created by PhpStorm.
 * User: anttih
 * Date: 15/09/2017
 * Time: 15.14
 */

class NewsLumberjack extends Lumberjack
{
    public function updateCMSFields(FieldList $fields)
    {
        $excluded = $this->owner->getExcludedSiteTreeClassNames();
        if (!empty($excluded)) {
            $pages = $this->getLumberjackPagesForGridfield($excluded)->sort("Created DESC");
            $gridField = new GridField(
                "ChildPages",
                $this->getLumberjackTitle(),
                $pages,
                $this->getLumberjackGridFieldConfig()
            );

            $tab = new Tab('ChildPages', $this->getLumberjackTitle(), $gridField);
            $fields->insertAfter($tab, 'Main');
        }

        return $fields;
    }
}
