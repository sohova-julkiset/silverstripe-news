<?php

class LatestNewsExtension extends DataExtension
{

    /**
     * Gets all news pages that are marked with the tags
     *
     * @return DataList
     */
    public function LatestNews()
    {
        if (in_array(
            get_class($this->owner),
            Config::inst()->get("LatestNewsExtension", 'excludepages')
        )) {
            return null;
        }
        return NewsPage::get()->sort('Date', 'DESC');
    }

    /**
     * Gets the first news arcihe for the language
     */
    public function NewsArchive()
    {
        return NewsArchive::get()->first();
    }
}
