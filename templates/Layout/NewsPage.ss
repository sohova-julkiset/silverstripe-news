<div class="main">
    <% cache 'MainInnerDefinition' %>
    <div class="main__inner<% if $HasSidebar %>  main__inner--sidebar<% end_if %>"><% end_cache %>
        <% cache 'MainContent' %>
        <main<% if $HasSidebar %> class="main__content"<% end_if %>>
            <% if $ContentTitle %>
                <header class="main__header">
                    <h1>$ContentTitle</h1>
                    <p class="main__header__date">
                        $Date.FormatI18N("%d.%m.%Y %H:%M")
                    </p>
                </header>
            <% end_if %>
            $Content
        </main>
        <% end_cache %>
        <% cache 'MainSidebar' %>
        <% if $HasSidebar %>
            <aside class="main__sidebar">
                <% include MainSidebar %>
            </aside>
        <% end_if %>
        <% end_cache %>
    </div>
</div>
<% if $Form %>
    <section class="main">
        <div class="main__inner">
            $Form
        </div>
    </section>
<% end_if %>
